# README #

Change the format of the shipping addres in the backend order grid

### How do I get set up? ###

1. clone the repository
2. create folder app/code/Mbs/OrderGrid when located at the root of the Magento site
3. copy the content of this repository within the folder
4. install the module php bin/magento setup:upgrade

Place a new order.
Go to order grid in the backend and verify the shipping address has the postcode before the city